This is a Composer-based installer for the [Lightning](https://www.drupal.org/project/lightning) Drupal distribution using [DDev](https://ddev.readthedocs.io/en/stable/). Welcome to the future!

## Get Started

After Cloning this repository:

Install [Composer](https://getcomposer.org/)
Install [Docker](https://www.docker.com/get-started) for your machine.
Install [DDev for Mac](https://ddev.readthedocs.io/en/stable/#homebrewlinuxbrew-macoslinux) or [DDev for Windows](https://ddev.readthedocs.io/en/stable/#installation-or-upgrade-windows)

```
$ cd ddevlightning
$ ddev start
$ ddev composer install --no-dev
$ ddev config
$ ddev restart
$ drush sql-drop -y
$ ddev restore-snapshot currentDB
$ ddev start
```

This will create a functioning Lightning site. Follow the prompts to set up your site.

Normally, Composer will install all dependencies into a `vendor` directory that
is _next_ to `docroot`, not inside it. This may create problems in certain
hosting environments, so if you need to, you can tell Composer to install
dependencies into `docroot/vendor` instead:

```
$ composer create-project acquia/lightning-project MY_PROJECT --no-install-dev
$ composer config vendor-dir docroot/vendor
$ cd MY_PROJECT
$ composer install
```

Either way, remember to keep the `composer.json` and `composer.lock` files that exist above `docroot` -- they are controlling your dependencies.

## Cloning the Database
Watch this video from [DDev](https://www.youtube.com/watch?v=Ax-HocnXNbc) on the snapshots feature. To use:

```
$ ddev snapshot --name=<Name the snapshot db>
```

## Maintenance

`drush make`, `drush pm-download`, `drush pm-update` and their ilk are the old-school way of maintaining your code base. Forget them. You're in Composer land now!

Let this handy table be your guide:

| Task                                            | Drush                                          | Composer                                           |
| ----------------------------------------------- | ---------------------------------------------- | -------------------------------------------------- |
| Installing a contrib project (latest version)   | `ddev drush pm-download PROJECT`               | `ddev composer require drupal/PROJECT`             |
| Installing a contrib project (specific version) | `ddev drush pm-download PROJECT-8.x-1.0-beta3` | `ddev composer require drupal/PROJECT:1.0.0-beta3` |
| Installing a javascript library (e.g. dropzone) | `ddev drush pm-download dropzone`              | `ddev composer require bower-asset/dropzone`       |
| Updating all contrib projects and Drupal core   | `ddev drush pm-update`                         | `ddev composer update`                             |
| Updating a single contrib project               | `ddev drush pm-update PROJECT`                 | `ddev composer update drupal/PROJECT`              |
| Updating Drupal core                            | `ddev drush pm-update drupal`                  | `ddev composer update drupal/core`                 |

The magic is that Composer, unlike Drush, is a _dependency manager_. If module `foo version: 1.0.0` depends on `baz version: 3.2.0`, Composer will not let you update baz to `3.3.0` (or downgrade it to `3.1.0`, for that matter). Drush has no concept of dependency management. If you've ever accidentally hosed a site because of dependency issues like this, you've probably already realized how valuable Composer can be.

But to be clear: it is still very helpful to use a site management tool like Drush or Drupal Console. Tasks such as database updates (`drush updatedb`) are still firmly in the province of such utilities. This installer will install a copy of Drush (local to the project) in the `bin` directory.

### Specifying a version

you can specify a version from the command line with:

    $ ddev composer require drupal/<modulename>:<version>

For example:

    $ ddev composer require drupal/ctools:3.0.0-alpha26
    $ ddev composer require drupal/token:1.x-dev

In these examples, the composer version 3.0.0-alpha26 maps to the drupal.org version 8.x-3.0-alpha26 and 1.x-dev maps to 8.x-1.x branch on drupal.org.

If you specify a branch, such as 1.x you must add -dev to the end of the version.

**Composer is only responsible for maintaining the code base**.

## Source Control

If you peek at the `.gitignore` we provide, you'll see that certain directories, including all directories containing contributed projects, are excluded from source control. This might be a bit disconcerting if you're newly arrived from Planet Drush, but in a Composer-based project like this one, **you SHOULD NOT commit your installed dependencies to source control**.

When you set up the project, Composer will create a file called `composer.lock`, which is a list of which dependencies were installed, and in which versions. **Commit `composer.lock` to source control!** Then, when your colleagues want to spin up their own copies of the project, all they'll have to do is run `composer install`, which will install the correct versions of everything in `composer.lock`.
